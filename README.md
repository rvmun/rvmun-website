**RVMUN Webpage**
-----------------
This repository contains the code used to design the RVMUN 2019 webpage. 
Contents have been updated. 

Package Management
-------------------
This repository uses [pip-tools](https://github.com/jazzband/pip-tools) to synchronise python packages across computers. To add a new Python Package:

1. Insert the name of the python package in ``requirements.in``.
2. Then run `pip-compile` so that ``requirements.txt`` can be updated.
3. Finally, run `pip-sync` to download the new packages.