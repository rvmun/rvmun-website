import re
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import RadioField, SubmitField, HiddenField, PasswordField, StringField, TextAreaField, MultipleFileField
from wtforms.fields import EmailField
from wtforms.validators import DataRequired, Email
from werkzeug.utils import secure_filename


class PersonalInfoForm(FlaskForm):
    email = EmailField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Enter')


class PasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField("Enter")


class ProtectedFilePasswordForm(PasswordForm):
    file_identifier = HiddenField(validators=[DataRequired()])


class ArticleForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    author = StringField('Author', validators=[DataRequired()])
    agency = StringField('Agency', validators=[DataRequired()])
    content = TextAreaField('Content', validators=[DataRequired()])
    image = FileField(validators=[FileRequired(), FileAllowed(['jpg', 'png'], 'Please upload an image.')])
    image_caption = StringField('Caption', validators=[DataRequired()])
    submit = SubmitField("Enter")

    def validate_content_markup(self):
        """
        Checks that there is only 1 placeholder in the message

        The placeholder is '{}'.
        :return: boolean
        """
        if not self.validate_on_submit():
            return False

        paragraphs = self.content.data.split('\n')
        paragraphs = tuple(filter(lambda x: x != "", paragraphs))
        placeholder_found = False
        for p in paragraphs:
            if p.strip() == '{}':
                if placeholder_found:
                    self.content.errors.append('There is more than 1 placeholder present.')
                    return False
                else:
                    placeholder_found = True

        if not placeholder_found:
            # manually add a placeholder at the bottom of the file
            self.content.data += '\n{}'

        return True
