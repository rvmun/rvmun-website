from flask import Flask
from flask_session import Session
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from flask_migrate import Migrate

app = Flask(__name__)
app.config.from_object(Config)
csrf = CSRFProtect(app)

# Database configurations
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Session configuration
Session(app)

from app import views, errors
