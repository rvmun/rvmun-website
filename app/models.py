from app import db
from datetime import datetime
from pathlib import Path
from os.path import splitext
from csv import reader
from sqlalchemy.orm import backref


class PressArticle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, nullable=False)
    author = db.Column(db.Text, nullable=False)
    agency = db.Column(db.Text, nullable=False)
    time = db.Column(db.DateTime, default=datetime.now)

    def __init__(self, title, author, agency):
        self.title = title
        self.author = author
        self.agency = agency

    def get_id(self):
        return self.id

    def get_title(self):
        return self.title

    def get_author(self):
        return self.author

    def get_agency(self):
        return self.agency

    def get_time(self):
        return '{:%d %b %Y - %I:%M %p}'.format(self.time)


# Helper functions to populate database
def validate_csv_filepath(filepath):
    path_instance = Path(filepath)
    if not path_instance.is_file():
        print('Invalid filepath provided')
        return False
    elif splitext(filepath)[1] != '.csv':
        print('CSV file required')
        return False
    else:
        return True
