import json
import requests
import pickle
import os
import re
from flask import Markup, session, redirect, url_for, flash
from datetime import date, datetime, timedelta
from functools import wraps
from cloudinary.uploader import upload
from app import db
from app.classes import Council, Director, Secretariat, CouncilTopic, Post, ArticleImage
from app.models import PressArticle


def load_sect():
    with open('app/protected/secretariat.json', encoding='utf8') as j:
        data = json.load(j)
    j.close()

    sect_dicts = data['secretariat']
    sect = []
    for d in sect_dicts:
        name = d['name']
        if name == "Vania Xu":
            position = ['Under-Secretary-General', 'Deputy Secretary-General']
            comm = ['Administration', 'Publicity']
        else:
            position = d['position'].split(' (')
            comm = position[-1][:-1] if len(position) > 1 else ''
            position = position[0]
        bio = list(map(lambda s: Markup(s) if '&mdash;' in s or '&frac34;' in s or '<del>' in s else s, d['bio'].split('\n')))
        sect.append(Secretariat(name, position, comm, bio))
    return sect


def load_councils():
    with open('app/protected/councils.json', encoding='utf8') as j:
        data = json.load(j)['councils']
    j.close()

    councils = []
    for d in data:
        council_dir = []
        for sub_d in d['directors']:
            bio = list(map(lambda s: Markup(s) if '&trade;' in s or '&mdash;' in s or '<strong>' in s or '<em>' in s else s, sub_d['bio'].split('\n')))
            council_dir.append(Director(sub_d['name'], sub_d['email'], bio))
        long = d['long']
        intro = list(map(lambda s: Markup(s) if '&mdash;' in s or '</ol>' in s else s, d['intro'].split('\n')))
        topics = []
        for topic in d['topics']:
            topic_intro = topic['intro'].split("\n")
            topics.append(CouncilTopic(topic['name'], topic_intro))
        councils.append(Council(d['council'], long, council_dir, topics, intro))
    return councils


def get_council_dir_info(council):
    councils = load_councils()
    directors = list(filter(lambda c: c.get_name() == council.upper(), councils))[0].get_directors()
    return directors


def get_council_topics(council):
    councils = load_councils()
    topics = list(filter(lambda c: c.get_name() == council.upper(), councils))[0].get_topics()
    return topics


def get_council_intro(council):
    councils = load_councils()
    intro = list(filter(lambda c: c.get_name() == council.upper(), councils))[0].get_intro()
    return intro


def get_council_long(council):
    councils = load_councils()
    long = list(filter(lambda c: c.get_name() == council.upper(), councils))[0].get_long()
    return long


def get_all_committee_descriptions():
    with open('app/protected/committees.json', encoding='utf8') as j:
        data = json.load(j)
    j.close()
    return data['committees']


def get_timeline():
    with open('app/protected/schedule.json', encoding='utf8') as j:
        data = json.load(j)
    j.close()
    return data


# For Instagram Updates
def get_profile_json():
    access_token = '1716078902.185194e.bc7f41327e914b2a9ea4fc46b8d41109'
    return requests.get('https://api.instagram.com/v1/users/self/media/recent/', params={'access_token': access_token}).json()


def get_post_id(url):
    if url[-1] == '/':
        url = url[:-1]

    return url.split('/')[-1]


def get_post(profile, n):
    post_json = profile['data'][n]
    created_time = int(post_json['created_time'])
    url = post_json['link']
    return (url, created_time)


def save_post_embed_html(url):
    filename = 'app/static/posts/{}.pkl'.format(get_post_id(url))
    if not os.path.exists(filename):
        post = requests.get('https://api.instagram.com/oembed', params={'url': url})
        embed_html = Markup(post.json()['html'])
        post_obj = Post(url, embed_html)
        with open(filename, 'wb') as f:
            pickle.dump(post_obj, f)
            f.close()


def load_post_embed_html(url):
    filename = 'app/static/posts/{}.pkl'.format(get_post_id(url))
    if os.path.exists(filename):
        with open(filename, 'rb') as f:
            post = pickle.load(f)
            f.close()
        return post.get_html()


def get_latest_posts():
    max_num = 20
    earliest_time = 1546473600
    i = 0
    posts = []
    profile = get_profile_json()
    done = False
    while not done and i < max_num:
        url, time = get_post(profile, i)
        done = time < earliest_time
        if not done:
            save_post_embed_html(url)
            posts.append(url)
            i += 1
    return posts


def get_latest_embed_html():
    posts = get_latest_posts()
    return list(map(load_post_embed_html, posts))


def validate_password(file_identifier, password):
    with open('app/protected/passes.json', encoding='utf8') as j:
        # key is identifier of file
        data = json.load(j)
    j.close()

    if file_identifier in data and password == data[file_identifier]:
        session[file_identifier] = True
        return True

    return False


def press_admin_login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'press_admin' not in session:
            return redirect(url_for('press_admin_login'))

        return f(*args, **kwargs)

    return wrap


def process_image(filepath, article_dir):
    # process image such that image is duplicated and cropped to 4:3 ratio
    uploaded_image = upload(filepath)
    image_url = uploaded_image['secure_url'].split('/')
    image_url[-2] = 'ar_16:9,c_crop'
    image_url = '/'.join(image_url)
    r = requests.get(image_url)

    with open(article_dir + 'image1_thumbnail.jpg', 'wb') as f:
        f.write(r.content)


def insert_article(form):
    article = PressArticle(form.title.data, form.author.data, form.agency.data)
    db.session.add(article)
    db.session.commit()

    article_dir = 'app/static/articles/{}/'.format(str(article.get_id()))
    os.makedirs(article_dir, exist_ok=True)

    image = form.image.data
    # ext = secure_filename(image.filename).split('.')[-1]
    image_filename = 'image1.jpg'
    image.save(article_dir + image_filename)
    process_image(article_dir + image_filename, article_dir)

    with open(article_dir + 'content.txt', 'w') as f:
        paragraphs = form.content.data.split('\n')
        for p in paragraphs:
            if p.strip() != '{}':
                f.write(p + '\n')
            else:
                f.write('{' + image_filename + '|' + form.image_caption.data + '}\n')


def get_article(article):
    article_dir = 'app/static/articles/{}/'.format(str(article.get_id()))
    with open(article_dir + 'content.txt') as f:
        article_data = f.read().split('\n')
        article_data = list(filter(lambda x: x != '', article_data))

    # assume all is text first
    article_data = list(map(lambda x: (True, x), article_data))

    # replace image caption placeholder with class
    regexp = re.compile(r"^{(?P<filename>[a-zA-Z0-9_ ]+\.[a-zA-Z]+)\|(?P<caption>.+)}$")
    for i in range(len(article_data)):
        match = regexp.match(article_data[i][1].strip())
        if match:
            article_data[i] = (False, ArticleImage(os.path.join('articles/{}/'.format(str(article.get_id())),
                                                                match.group('filename')),
                                                   match.group('caption')))

    return article_data


def regenerate_all_image_thumbnails():
    articles = PressArticle.query.all()
    for article in articles:
        article_dir = 'app/static/articles/{}/'.format(str(article.get_id()))
        process_image(article_dir + 'image1.jpg', article_dir)
