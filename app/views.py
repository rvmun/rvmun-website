from flask import render_template, abort, redirect, url_for, make_response, request, session, send_from_directory
from sqlalchemy import desc
from datetime import date, datetime
from os import path, walk
from app import app, db
from app.helpers import load_sect, get_council_dir_info, get_council_topics, get_council_intro, get_council_long, \
    get_all_committee_descriptions, get_timeline, get_latest_embed_html, validate_password, \
    press_admin_login_required, insert_article, get_article
from app.forms import PersonalInfoForm, PasswordForm, ProtectedFilePasswordForm, ArticleForm
from app.models import PressArticle

ALL_COUNCILS = ('WHO', 'ECOSOC', 'UNESCO', 'SOCHUM', 'SPECPOL', 'UNSC', 'EC', 'HCC')


# Set Countdown Cookie
@app.route('/enter/')
def set_cd_cookie():
    resp = make_response(redirect(url_for('home')))
    resp.set_cookie('countdown', max_age=1800)  # cookie expires after 30 mins
    return resp


# Coming soon...
@app.route('/comingsoon/')
def coming_soon():
    return render_template('coming_soon.html')


# Index
@app.route('/')
def home():
    countdown_datetime = datetime.strptime("2019-03-18 09:00", "%Y-%m-%d %H:%M")
    now = datetime.now()
    if 'countdown' not in request.cookies and now < countdown_datetime:
        return render_template('countdown.html')
    return render_template('home.html')


# About Us
@app.route('/about/')
def about():
    # since there are some roles (HACAS President and Sec Gen) that do not have an assigned committee, so
    # adding a '':'' empty key-value pair circumvents this problem
    comms = {'Academics': 'Acads', 'Administration': 'Admin', 'Publicity': 'Publi',
             'Conference': 'Conference', 'Logistics': 'Logi', 'Internal Staff': 'IS', 'Press Corps': 'PressCorps', '': ''}
    positions = {'HACAS President': 'HACASPres', 'Secretary-General': 'SecGen',
                 'Deputy Secretary-General': 'DSG', 'Under-Secretary-General': 'USG'}
    sect = load_sect()
    descriptions = get_all_committee_descriptions()

    return render_template('about_us.html', comms=comms, positions=positions, sect=sect, descriptions=descriptions)


# Register
@app.route('/register/')
def register():
    return render_template('register.html')


# Councils
@app.route('/councils/')
def councils_home():
    councils_long = {council: get_council_long(council) for council in ALL_COUNCILS}
    topics = {council: get_council_topics(council) for council in ALL_COUNCILS}
    intros = {council: get_council_intro(council) for council in ALL_COUNCILS}
    return render_template('councils_home.html', councils=ALL_COUNCILS, longs=councils_long, topics=topics, intros=intros)


# Council Page
@app.route('/council/<council>')
def council_page(council):
    if council.upper() not in ALL_COUNCILS:
        abort(404)

    topics = get_council_topics(council)
    directors = get_council_dir_info(council)
    return render_template('council_page.html', council=council, directors=directors, topics=topics)


# Conference
@app.route('/conference/')
def conference():
    return render_template('conference.html', timeline=get_timeline())


# For Delegates
@app.route('/resources/', methods=['GET', 'POST'])
def for_delegates():
    form = ProtectedFilePasswordForm()
    if form.validate_on_submit():
        file_identifier = form.file_identifier.data
        if validate_password(file_identifier, form.password.data):
            return redirect(url_for('retrieve_protected_file', identifier=file_identifier))
        else:
            form.password.errors.append('Password is incorrect.')

    councils_long = {council: get_council_long(council) for council in ALL_COUNCILS}
    return render_template('resources.html', councils=ALL_COUNCILS, longs=councils_long, form=form)


@app.route('/updates/')
def updates():
    embed_htmls = get_latest_embed_html()
    return render_template('updates.html', embed_htmls=embed_htmls)


@app.route('/merchandise')
def merchandise():
    return render_template('merchandise.html')


@app.route('/protected/<path:identifier>')
def retrieve_protected_file(identifier):
    full_path = path.join(app.root_path, 'protected', identifier)
    if identifier in session and path.isfile(full_path):
        return send_from_directory(path.dirname(full_path), path.basename(full_path))
    else:
        abort(404)


@app.route('/press', methods=['GET', 'POST'])
def press():
    form = ProtectedFilePasswordForm()
    if form.validate_on_submit():
        file_identifier = form.file_identifier.data
        if validate_password(file_identifier, form.password.data):
            return redirect(url_for('retrieve_protected_file', identifier=file_identifier))
        else:
            form.password.errors.append('Password is incorrect.')

    articles = PressArticle.query.order_by(desc('id')).all()

    return render_template('press.html', form=form, council="PressCorps", articles=articles)


@app.route('/press/<int:article_id>')
def get_press_article(article_id):
    article = PressArticle.query.get(article_id)
    if not article:
        abort(404)

    article_data = get_article(article)

    return render_template('article.html', article=article, article_data=article_data)


@app.route('/press/admin/login', methods=['GET', 'POST'])
def press_admin_login():
    form = PasswordForm()
    if form.validate_on_submit() and form.password.data == app.config.get('PRESS_ADMIN_PASSWORD'):
        session['press_admin'] = True
        return redirect(url_for('press_admin_home'))
    elif form.validate_on_submit():
        form.password.errors.append('Password is incorrect.')

    return render_template('press_admin/login.html', form=form)


@app.route('/press/admin/home')
@press_admin_login_required
def press_admin_home():
    articles = PressArticle.query.order_by(desc('id')).all()

    return render_template('press_admin/home.html', articles=articles)


@app.route('/press/admin/add', methods=['GET', 'POST'])
@press_admin_login_required
def add_press_article():
    form = ArticleForm()
    if form.validate_on_submit() and form.validate_content_markup():
        insert_article(form)
        return redirect(url_for('press_admin_home'))

    return render_template('press_admin/form.html', func='Add', form=form)


@app.route('/press/admin/delete/<int:article_id>', methods=['GET', 'POST'])
@press_admin_login_required
def delete_press_article(article_id):
    # load the article
    article = PressArticle.query.get(article_id)
    if not article:
        abort(404)

    db.session.delete(article)
    db.session.commit()

    return redirect(url_for('press_admin_home'))


@app.route('/gallery')
def gallery_home():
    return render_template('gallery_home.html')


@app.route('/gallery/<int:day>')
def gallery(day):
    if day < 1 or day > 3:
        abort(404)

    dir_path = 'app/static/img/event_pictures/{}'.format(day)
    filepaths = []
    for r, d, f in walk(dir_path):
        for file in f:
            if '.jpg' in file or '.png' in file:
                filepaths.append('img/event_pictures/{}/{}'.format(day, file))

    # sort the photos by filename
    filepaths = sorted(filepaths, key=lambda x: int(x.split('/')[-1][:-4]))  # key retrieves filename and removes extension
    return render_template('gallery.html', day=day, filepaths=filepaths)
