// Timer
function pad_number(number) {
    // pads single digit numbers to double digit
    if (number < 10) {
        return "0" + number.toString()
    } else {
        return number.toString()
    }
}

function update_timer() {
    let countDownDate = new moment("2019-03-18T09:00:00", moment.HTML5_FMT.DATETIME_LOCAL_MS);
    let currDate = new moment().utcOffset(8);

    if (currDate < countDownDate) {
        let time_difference = moment.duration(countDownDate.diff(currDate));
        let days = Math.floor(time_difference.asDays());
        let hours = Math.floor(time_difference.asHours() - days * 24);
        let mins = Math.floor(time_difference.asMinutes() - days * 24 * 60 - hours * 60);
        let seconds = Math.ceil(time_difference.asSeconds() - days * 24 * 60 * 60 - hours * 60 * 60 - mins * 60);

        // t-minus appears in navbar, the other appears in countdown
        if (document.getElementById('t-minus') !== null) {
            document.getElementById('t-minus').innerHTML = pad_number(days) + "d " + pad_number(hours) + "h " +
                pad_number(mins) + "m " + pad_number(seconds) + "s";
        }
        if (document.getElementById('sec') !== null) {
            document.getElementById('day').innerHTML = pad_number(days);
            document.getElementById('hour').innerHTML = pad_number(hours);
            document.getElementById('min').innerHTML = pad_number(mins);
            document.getElementById('sec').innerHTML = pad_number(seconds);
        }
    } else {
        if (document.getElementById('t-minus') !== null) {
            document.getElementById('t-minus').innerHTML = "";
        }
    }
}

setInterval(update_timer, 1000);