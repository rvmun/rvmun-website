function submit_answers() {
    let options = document.getElementsByClassName('qn-option');
    let num = options.length;
    for (var i = 0; i < num; i++) {
        if (options[i].checked) {
            document.getElementById('quizForm').submit.click();
        }
    }

    window.location = "/aptitude-test/quiz/next"
}

function update_timer() {
    //let now = new moment().utcOffset(8);
    //let time_left = 65 - moment.duration(now.diff(start)).asSeconds();
    let time_left = parseInt(document.getElementById('time').value, 10) - 1;
    if (time_left <= 0) {
        clearInterval(timer_func);

        Swal.fire({
            text: "Time's up!",
            button: "Next",
            allowOutsideClick: false,
            allowEscapeKey: false,
            onClose: submit_answers
        })
    }

    document.getElementById('quiz-timer').innerHTML = pad_number(time_left) + ' seconds left';
    document.getElementById('time').value = time_left;
}

let timer_func = setInterval(update_timer, 1000);