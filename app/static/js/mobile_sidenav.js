$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

// Fix for mobile About Us dropdown list as anchors do not close the sidebar
$("#AboutSubMenu li a").click(function(e) {
    $("#wrapper").toggleClass("toggled");
});
