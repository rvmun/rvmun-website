class Council:
    def __init__(self, name, long, directors, topics, intro):
        self.name = name
        self.long = long
        self.directors = directors
        self.topics = topics
        self.intro = intro

    def get_name(self):
        return self.name

    def get_long(self):
        return self.long

    def get_directors(self):
        return self.directors

    def get_topics(self):
        return self.topics

    def get_intro(self):
        return self.intro

    def __str__(self):
        director_names = list(map(lambda d: d.get_name(), self.directors))
        return "{}: Directors: {}, Topics: {}".format(self.name, '; '.join(director_names), ', '.join(self.topics))


class CouncilTopic:
    def __init__(self, name, intro):
        self.name = name
        self.intro = intro

    def get_name(self):
        return self.name

    def get_intro(self):
        return self.intro


class Director:
    def __init__(self, name, email, bio):
        self.name = name
        self.email = email
        self.bio = bio

    def get_name(self):
        return self.name

    def get_email(self):
        return self.email

    def get_bio(self):
        return self.bio


class Secretariat:
    def __init__(self, name, position, comm, bio):
        self.name = name
        self.position = position
        self.comm = comm
        self.bio = bio

    def get_name(self):
        return self.name

    def get_position(self):
        if self.name == "Vania Xu":
            return 'Deputy Secretary-General'
        return self.position

    def get_title(self):
        if self.name == "Vania Xu":
            return 'Under-Secretary-General (Administration)\nDeputy Secretary-General (Publicity)'
        return '{}{}'.format(self.position, ' ({})'.format(self.comm) if self.comm else '')

    def get_comm(self):
        if self.name == "Vania Xu":
            return 'Publicity'
        return self.comm

    def get_bio(self):
        return self.bio

    def __str__(self):
        if self.name == "Vania Xu":
            return "Vania Xu (Under-Secretary-General (Administration), Deputy Secretary-General (Publicity))"
        return "{} ({})".format(self.name, self.get_title())


class Post:
    def __init__(self, url, html):
        self.url = url
        self.html = html

    def get_url(self):
        return self.url

    def get_html(self):
        return self.html


class ArticleImage:
    def __init__(self, filepath, caption):
        self.filepath = filepath
        self.caption = caption

    def get_path(self):
        return self.filepath

    def get_caption(self):
        return self.caption
