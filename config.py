import os
import cloudinary


class Config:
    DEBUG = True
    SECRET_KEY = os.environ.get('SECRET_KEY') or "haiyunmanman"

    # Database Configurations
    POSTGRES = {
        'user': os.environ.get('DB_USERNAME') or "rvmun",
        'pw': os.environ.get('DB_PASSWORD') or "shanekoh",
        'db': os.environ.get('DB_NAME') or "rvmun",
        'host': 'localhost',
        'port': '5432',
    }

    SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{pw}@{host}:{port}/{db}'.format(**POSTGRES)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True

    # Session Management
    SESSION_TYPE = "redis"
    SESSION_COOKIE_SECURE = False
    PERMANENT_SESSION_LIFETIME = 3600  # 60 mins

    PRESS_ADMIN_PASSWORD = os.environ.get('PRESS_ADMIN_PASSWORD') or "rvmun2019"

    cloudinary.config(
        cloud_name=os.environ.get('CLOUDINARY_CLOUD_NAME') or "rvmun2019",
        api_key=os.environ.get('CLOUDINARY_CLOUD_KEY'),
        api_secret=os.environ.get('CLOUDINARY_CLOUD_SECRET')
    )

    FREEZER_DESTINATION = 'docs'  # for github-pages
